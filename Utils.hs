module Utils where

import qualified Data.Map as M
import Network.URL
import Network.HTTP

getContent :: URL -> IO String
getContent url = simpleHTTP req >>= getResponseBody
  where
  req = getRequest $ exportURL url

mapPair :: ((a -> b), (a -> c)) -> a -> (b, c)
mapPair (f1, f2) x = (f1 x, f2 x)

stringLookup :: String -> M.Map String String -> String
stringLookup s m = maybe "" id $ M.lookup s m


