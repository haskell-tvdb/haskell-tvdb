module Types
where

import Data.Time.Calendar

type ApiKey = String
type Path = String

type SeriesID = Int
type SeasonID = Int
type EpisodeID = Int

data Series = Series { seriesID :: SeriesID
                     , seriesName :: String
                     , seasons :: [Season]
                     } deriving (Show)

data Season = Season { seasonID :: SeasonID
                     , seasonNumber :: Int
                     , episodes :: [Episode]
                     } deriving (Show)

data Episode = Episode { epID :: EpisodeID
                       , epName :: String
                       , epNumber :: Int
                       , epSeasonNumber :: Int
                       , airDate :: Day
                       }

instance Show Episode where
  show e = name ++ " " ++ show season ++ "x" ++ show number
    where
    name = epName e
    number = epNumber e
    season = epSeasonNumber e
