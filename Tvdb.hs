{- dependencies:
 - http
 - xml
 - url
 -}
module Tvdb (
    getServerTime
  , getSeriesID
  , getEpisodeByAirDate
  , getTodayEpisode
  , getTodayEpisodeByName

  , ParseResult
  , Episode(..)
  , Season(..)
  , Series(..)
  )
where

import Types
import Parser
import Utils

import Control.Monad
import Data.String.Utils (replace)
import qualified Data.Map as M
import qualified Data.Time as T
import qualified Network.HTTP as HTTP
import qualified Network.URL as URL

baseURL = URL.URL { URL.url_type = URL.Absolute
                                     (URL.Host 
                                       { URL.protocol = URL.HTTP False
                                       , URL.host = "thetvdb.com"
                                       , URL.port = Nothing
                                       })
                  , URL.url_path = ""
                  , URL.url_params = []
                  }

makeURL :: String -> [(String, String)] -> URL.URL
makeURL path params = baseURL { URL.url_path = path
                              , URL.url_params = params
                              }

getServerTime :: IO (ParseResult T.Day)
getServerTime = parseServerTime `liftM` 
  getContent (makeURL "/api/Updates.php" [("type", "none")])

getSeriesID :: String -> IO (ParseResult Int)
getSeriesID name = parseSeriesID `liftM`
  getContent (makeURL "/api/GetSeries.php"
                      [("seriesname", name)])

getEpisodeByAirDate :: ApiKey -> SeriesID -> T.Day -> IO (ParseResult Episode)
getEpisodeByAirDate key sid date = parseEpisode `liftM`
  getContent (makeURL "/api/GetEpisodeByAirDate.php"
                      [("apikey", key)
                      ,("seriesid", show sid)
                      ,("airdate", T.showGregorian date)
                      ])
  
getTodayEpisode :: ApiKey -> SeriesID -> IO (ParseResult Episode)
getTodayEpisode key sid = (T.localDay . T.zonedTimeToLocalTime) `liftM` T.getZonedTime >>=
                            getEpisodeByAirDate key sid

getTodayEpisodeByName :: ApiKey -> String -> IO (ParseResult Episode)
getTodayEpisodeByName key sname = getSeriesID sname >>= \x ->
  case x of
    Left e -> return $ Left e
    Right i -> getTodayEpisode key i
