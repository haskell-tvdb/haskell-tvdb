module Parser
where

import Utils (mapPair, stringLookup)
import Types

import Control.Monad
import Text.XML.Light
import Text.Regex.Posix
import Data.Time.Calendar
import qualified Data.Map as M

type ParseResult a = Either String a

parseTime :: String -> Day
parseTime s = fromGregorian year month day
  where
  stringdate = concat (s =~ "[0-9]+" :: [[String]])
  year = read $ head stringdate
  month = read $ head . tail $ stringdate
  day = read $ last stringdate

elemToMap :: Element -> M.Map String String
elemToMap = M.fromList . map p . elChildren
  where
  p e = mapPair (qName . elName, pContent . head . elContent) e
  pContent (Text cd) = cdData cd
  pContent _ = ""

extractXMLContent :: String -> String -> ParseResult (M.Map String String)
extractXMLContent parentElem elem = 
  let parsed = parseXMLDoc parentElem
  in case parsed >>= findChild (QName "Error" Nothing Nothing) of
    -- there is an error: parse the error string
    Just e -> Left $ let Text t = head . elContent $ e
                      in "Error while parsing " ++ elem ++ ": " ++ cdData t
    -- no error: assume expected xml structure
    Nothing -> Right $ maybe M.empty
                              elemToMap
                              (parsed >>= findChild (QName elem Nothing Nothing))

{- the server time xml looks like this:
 - <Items>
 -   <Time>time</Time>
 - </Items>
 -}
parseServerTime :: String -> ParseResult Day
parseServerTime s = (parseTime . stringLookup "Time") `liftM` extractXMLContent s "Time"

{- the xml looks like this:
 - <Data>
 -   <Series>
 -     <SeriesName>name</SeriesName>
 -     <id>1234</id>
 -   </Series>
 - </Data>
 -}

parseSeriesID :: String -> ParseResult Int
parseSeriesID s = (read . stringLookup "id") `liftM` extractXMLContent s "Series"

{- the xml looks like this:
 - <Data>
 -   <Episode>
 -     <id>
 -     <Combined_episodenumber>
 -     <Combined_seasonnumber>
 -     <EpisodeName>
 -     <EpisodeNumber>
 -     <FirstAired>
 -     <SeasonNumber>
 -     <seasonid>
 -     <seriesid>
 -   </Episode>
 - </Data>
 -}
parseEpisode :: String -> ParseResult Episode
parseEpisode s = toEpisode `liftM` extractXMLContent s "Episode"
  where
  toEpisode m = let eid = read $ stringLookup "id" m
                    name = stringLookup "EpisodeName" m
                    num = read $ stringLookup "EpisodeNumber" m
                    date = parseTime $ stringLookup "FirstAired" m
                    snum = read $ stringLookup "SeasonNumber" m
                in Episode { epID = eid
                           , epName = name
                           , epNumber = num
                           , epSeasonNumber = snum
                           , airDate = date
                           }

