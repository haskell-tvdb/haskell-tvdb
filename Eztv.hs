{- eztv.it downloader
 - search for episode:
 - url: http://www.ezrss.it/search/index.php
 - arguments: 
 -  show_name <string>
 -  show_name_exact (true|false)
 -  season <number>
 -  episode <number>
 -  mode (rss)
 -}

module Eztv
  ( downloadEpisode
  , module Tvdb
  )
where

import Tvdb
import Utils
import Text.XML.Light
import Control.Monad
import Network.HTTP
import qualified Network.URL as URL

baseURL = URL.URL { URL.url_type = URL.Absolute host
                  , URL.url_path = "search/index.php"
                  , URL.url_params = []
                  }
  where
  host = URL.Host { URL.protocol = URL.HTTP False
                  , URL.host = "ezrss.it"
                  , URL.port = Nothing
                  }

downloadEpisode :: String -> Episode -> IO ()
downloadEpisode seriesName ep = download $ baseURL { URL.url_params = 
                                 [("show_name", seriesName)
                                 ,("show_name_exact", "true")
                                 ,("season", show $ epSeasonNumber ep)
                                 ,("episode", show $ epNumber ep)
                                 ,("mode", "rss")
                                 ]}
  where
  download u = parseTorrent `liftM` getContent u >>= \x ->
                 case x of
                   Left e -> putStrLn $ show ep ++ ": " ++ e
                   Right x -> downloadLink x
  -- download u = putStrLn $ URL.exportURL u

{- <rss>
 -   <channel>
 -     <item>
 -       <link></link>
 -     </item>
 -   </channel>
 - </rss>
 -}
parseTorrent :: String -> ParseResult String
parseTorrent s =
  case parsed >>= findChild (QName "channel" Nothing Nothing)
              >>= findChild (QName "item"    Nothing Nothing)
              >>= findChild (QName "link"    Nothing Nothing) of
    Just e -> Right $ pContent . head . elContent $ e
    Nothing -> Left $ "Cannot parse rss"
    where
    parsed = parseXMLDoc s
    pContent (Text t) = cdData t

downloadLink :: String -> IO ()
downloadLink link = do
  resp <- simpleHTTP . getRequest . encode $ link
  case resp of
    Left e -> putStrLn $ "error downloading episode: " ++ link
    Right r -> putStrLn $ "RESPONSE:\n" ++ rspBody r

encode = URL.encString False (`elem` (['a'..'z'] ++ ['A'..'Z']
                                   ++ ['0'..'9'] ++ [':', '/', '.']))
