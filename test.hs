{- fetches and lists today's episodes of the shows listed
 - in the "shows.txt" file
 -}

module Main
where

import Tvdb
import Eztv
import Control.Applicative
import Control.Monad
import Data.Time
import System.IO

apikey = "B6A4552DF2DCF58A"

today = (localDay . zonedTimeToLocalTime) `liftM` getZonedTime
yesterday = addDays (-1) `liftM` today
twodaysago = addDays (-2) `liftM` today
tomorrow = addDays (1) `liftM` today

days = [yesterday, today, tomorrow]

listShows = do
  shows <- (filter (not . null) . lines) `liftM` readFile "shows.txt"
  forM_ days $ \d -> do
    day <- d
    putStrLn $ "Episodes on " ++ show day
    putStrLn   "======================"

    forM_ shows $ \name -> do
      
      sid <- getSeriesID name >>= \x ->
               case x of
                 Left e -> (putStrLn $ "Invalid show: " ++ name) >> return 0
                 Right y -> return y

      res <- getEpisodeByAirDate apikey sid day
      case res of
        Right x -> putStrLn $ name ++ " ==> " ++ show x
        Left e -> return ()

    putStrLn   "======================"

testEztvDownload = do
  sid <- getSeriesID "The Big Bang Theory" >>= \x ->
           case x of
             Left e -> putStrLn "Invalid show" >> return 0
             Right y -> return y

  putStrLn $ show sid

  ep <- getEpisodeByAirDate apikey sid =<< today
  case ep of
    Right e -> downloadEpisode "The Big Bang Theory" e
    Left e  -> putStrLn "No episode found" >> return ()

main = testEztvDownload
